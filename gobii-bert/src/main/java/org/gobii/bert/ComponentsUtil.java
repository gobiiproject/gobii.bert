package org.gobii.bert;

import java.io.*;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;
import java.util.Random;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.gobii.bert.matrix.Matrix;
import org.gobii.bert.matrix.Row;
import org.postgresql.jdbc.PgArray;
import org.springframework.http.*;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import ernie.core.Logger;

public class ComponentsUtil {

	private static final String PAYLOAD_ENVELOPE_JSON_TEMPLATE = "{\"payload\": {\"data\": %s}}";

	public static String buildParamString(String ... params) {

		if (params.length % 2 != 0) {
			throw new RuntimeException("Number of params must be even");
		}

		if (params.length == 0) {
			return "";
		}

		StringBuilder s = new StringBuilder();
		s.append("?");
		for (int i = 0 ; i < params.length ; i += 2) {
			s.append(params[i]);
			s.append("=");
			s.append(params[i+1]);

			if ((i + 2) < params.length) {
				s.append("&");
			}
		}

		return s.toString();
	}

	public static <T> ResponseEntity<T> get(String url, HttpHeaders headers, Class<T> as, Object body, String ... params) {

		HttpEntity<Object> request = new HttpEntity<>(body, headers);

		return new RestTemplate().exchange(buildUri(url, params), HttpMethod.GET, request, as);
	}

	public static <T> ResponseEntity<T> get(String url, HttpHeaders headers, Class<T> as, String ... params) {
		return get(url, headers, as, null, params);
	}

	public static <T> ResponseEntity<T> post(String url, HttpHeaders headers, Class<T> as, Object body, String ... params) {
		HttpEntity<Object> request = new HttpEntity<>(body, headers);

		return new RestTemplate().exchange(buildUri(url, params), HttpMethod.POST, request, as, params);
	}

	public static <T> ResponseEntity<T> post(String url, HttpHeaders headers, Class<T> as, String ... params) {
		return post(url, headers, as, null, params);
	}

	public static String buildUri(String url, String ... params) {

		return url + buildParamString(params);
	}

	public static String makeEnvelopeJson(String template, Object ... params) {
		return String.format(PAYLOAD_ENVELOPE_JSON_TEMPLATE,
				String.format(template, params));
	}

	public static JsonNode getIn(JsonNode node, Object ... path) {

		for (Object s : path) {
			if (s instanceof Integer) {
				node = node.get((Integer) s);
			} else if (s instanceof String) {
				node = node.get((String) s);
			}
		}

		return node;
	}

	public static <T> T as(Object object, Class<T> c) {
		return new ObjectMapper().convertValue(object, c);
	}

	public static HttpHeaders headers(String ... headers) {

		if (! (headers.length % 2 == 0)) {
			throw new RuntimeException("Number of headers must be even");
		}

		HttpHeaders h = new HttpHeaders();
		for (int i = 0 ; i < headers.length ; i+=2) {
			h.add(headers[i], headers[i+1]);
		}
		return h;
	}

	public static <T> T result(ResponseEntity<JsonNode> response, Class<T> as) {
		return new ObjectMapper().convertValue(getIn(response.getBody(), "payload", "data", 0), as);
	}

	public static void rm(String path) {
		File file = new File(path);
		if (file.exists()) {
			file.delete();
		}
	}

	public static String randomString() {
		return new Date().getTime() + "" + new Random().nextInt();
	}

	public static void printHttpError(HttpStatusCodeException e) throws RuntimeException {
		try {
			Logger.error(e.getResponseBodyAsString().replace("\\n", "\n"));
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	public static Matrix<String> tsvToMatrix(String tsv) {

		String[] lines = tsv.split("(\r)?\n");

		List<String[]> cells = new ArrayList<>();

		for (String line : lines) {
			cells.add(line.split("\\t"));
		}

		int height = cells.size();
		int width = cells.stream().map(arr -> arr.length).max(Comparator.comparingInt(x -> x)).get();

		Matrix<String> matrix = new Matrix<>(height, width);

		for (int i = 0 ; i < cells.size() ; i++) {
			for (int j = 0 ; j < cells.get(i).length ; j++) {
				matrix.set(i, j, cells.get(i)[j]);
			}
		}

		return matrix;
	}

	public static <T> String matrixToTsv(Matrix<T> matrix) {

		return
			matrix.rows()
				  .stream()
				  .map(r -> r.cells()
							 .stream()
							 .map(Objects::toString)
							 .collect(Collectors.joining("\t")))
				  .collect(Collectors.joining("\n")) + "\n";
	}

	public static boolean resultSetHasColumn(ResultSet rs, String columnName) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		for (int x = 1; x <= columns; x++) {
			if (columnName.equals(rsmd.getColumnName(x))) {
				return true;
			}
		}
		return false;
	}

	public static Object pgArrayToList(PgArray array) throws SQLException {
		return Arrays.asList((Object[]) array.getArray());
	}

	public static List<Map<String, Object>> readResultSet(ResultSet rs) throws SQLException {

		List<Map<String, Object>> result = new LinkedList<>();

		try {
			while (rs.next()) {
				Map<String, Object> row = new HashMap<>();
				int numColumns = rs.getMetaData().getColumnCount();
				for (int i = 0; i < numColumns; i++) {
					Object obj = rs.getObject(i + 1);
					if (obj instanceof PgArray) {
						obj = pgArrayToList((PgArray) obj);
					}
					row.put(rs.getMetaData().getColumnName(i + 1), obj);
				}
				result.add(row);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}

		return result;
	}

	public <S,T> Map<S,T> selectKeys(Map<S,T> m, Collection<S> keys) {
		Map<S,T> ret = new HashMap<>();

		for (S s : keys) {
			ret.put(s, m.get(s));
		}

		return m;
	}

	public static <S,T> S lastKey(LinkedHashMap<S, T> m) {
		return m.entrySet().stream().reduce((x,y) -> y).get().getKey();
	}

	public static <S> S or(S ... ss) {
		for (S s : ss) {
			if (! (s == null || Boolean.FALSE.equals(s))) {
				return s;
			}
		}
		return ss[ss.length - 1];
	}


	public static String str(Object any) {
		return any == null ? "" : any.toString();
	}

	public static String rpad(Integer n, String s, String p) {
		StringBuilder sb = new StringBuilder(s);

		for (int i = 0 ; i < n - s.length() ; i += p.length()) {
			sb.append(p);
		}

		return sb.toString();
	}

}
