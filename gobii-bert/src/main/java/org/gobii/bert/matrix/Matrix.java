package org.gobii.bert.matrix;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Matrix<T> {

	private List<List<T>> data;

	public Matrix() {
		data = new ArrayList<>();
	}

	public Matrix(int height, int width) {
		data = new ArrayList<>(height);
		for (int i = 0 ; i < height ; i++) {
			data.add(new ArrayList<T>(width));
			for (int j = 0 ; j < width ; j++) {
				data.get(i).add(null);
			}
		}
	}

	public T get(int i, int j) {

		if (i > data.size() || j > data.get(i).size()) {
			return null;
		}

		return data.get(i).get(j);
	}

	public void set(int i, int j, T t) {

		for (int k = data.size() ; k < i+1 ; k++) {
			this.data.add(new ArrayList<>());
		}

		for (int k = data.get(i).size() ; k < j+1 ; k++) {
			this.data.add(null);
		}

		data.get(i).set(j, t);
	}

	public int height() {
		return this.data.size();
	}

	public int width() {
		return this.data.stream().map(List::size).max(Integer::compareTo).get();
	}

	public Matrix<T> slice(int i, int j, int height, int width) {
		Matrix<T> matrix = new Matrix<>(height, width);
		for (; i < height && i < this.height() ; i++) {
			for (; j < width && j < this.width() ; j++) {
				matrix.set(i, j, this.get(i, j));
			}
		}
		return matrix;
	}

	public Matrix<T> slice(int i, int j) {
		return slice(i, j, height() - i, width() - j);
	}

	public Column<T> getColumn(int j) {
		Column<T> column = new Column<>(this.height());
		for (int i = 0 ; i < height() ; i++) {
			column.set(i, this.get(i, j));
		}
		return column;
	}

	public Row<T> getRow(int i) {
		Row<T> row = new Row<>(this.width());
		for (int j = 0 ; j < width() ; j++) {
			row.set(j, this.get(i, j));
		}
		return row;
	}

	public List<Column<T>> columns() {
		final int width = this.width();
		List<Column<T>> columns = new ArrayList<>(width);
		for (int i = 0 ; i < width ; i++) {
			columns.add(this.getColumn(i));
		}

		return columns;
	}

	public List<Row<T>> rows() {
		final int height = this.height();
		List<Row<T>> rows = new ArrayList<>(height);
		for (int i = 0 ; i < height ; i++) {
			rows.add(this.getRow(i));
		}

		return rows;
	}

	public List<T> cells() {

		final int height = this.height(),
				   width = this.width();

		List<T> cells = new ArrayList<>(height * width);

		for (int i = 0 ; i < height ; i++) {
			for (int j = 0 ; j < width ; j++) {
				cells.add(this.get(i,j));
			}
		}
		return cells;
	}

	public Matrix<T> transpose() {
		Matrix<T> m = new Matrix<T>() {
			@Override
			public T get(int i, int j) {
				return data.get(j).get(i);
			}

			@Override
			public void set(int i, int j, T t) {
				data.get(j).set(i, t);
			}
		};

		m.data = this.data;

		return m;
	}

	public Matrix<T> sub(int i, int j) {
		return sub(i, j, height() - i + 1, width() - j + 1);
	}

	public Matrix<T> sub(int i, int j, int height, int width) {

		Matrix<T> sub = new Matrix<>(height, width);

		for (; i < height ; i++) {
			for (; j < width ; j++) {
				sub.set(i, j, this.get(i, j));
			}
		}

		return sub;
	}

}
