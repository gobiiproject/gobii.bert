package org.gobii.bert.matrix;

public interface Vector<T> {

	T get(int i);

	void set(int i, T t);

	int length();

}
