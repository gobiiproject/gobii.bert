package org.gobii.bert.matrix;

import java.util.Arrays;
import java.util.List;

public class Column<T> extends Matrix<T> implements Vector<T> {

	public Column(int length) {
		super(length, 1);
	}

	public Column(T[] ts) {
		this(Arrays.asList(ts));
	}

	public Column(List<T> ts) {
		this(ts.size());
		for (int i = 0 ; i < ts.size() ; i++) {
			this.set(i, ts.get(i));
		}
	}

	@Override
	public T get(int i) {
		return super.get(i, 0);
	}

	@Override
	public void set(int i, T t) {
		super.set(i, 0, t);
	}

	@Override
	public int length() {
		return super.height();
	}

	public Column<T> slice(int i) {
		Column<T> m = new Column<>(this.height() - i);
		for (int j = 0; j < m.height() ; j++, i++) {
			m.set(j, this.get(i));
		}

		return m;
	}
}
