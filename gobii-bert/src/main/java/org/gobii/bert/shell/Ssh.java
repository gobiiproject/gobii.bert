package org.gobii.bert.shell;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import ernie.core.Logger;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import org.apache.commons.io.IOUtils;
import org.gobii.bert.Util;
import static org.gobii.bert.ComponentsUtil.*;

public class Ssh implements Shell {

	private String host;
	private Integer port = 22;
	private String user;
	private String password;

	public Ssh(String host, String user, String password) {

		if (host.contains(":")) {
			String[] toks = host.split(":");
			this.host = toks[0];
			this.port = Integer.parseInt(toks[1]);
		} else {
			this.host = host;
		}

		this.user = user;
		this.password = password;
	}

	public Session session() throws JSchException {

		JSch jsch = new JSch();

		Session session = jsch.getSession(user, host, port);

		java.util.Properties config = new java.util.Properties();
		config.put("StrictHostKeyChecking", "no");
		session.setConfig(config);


		session.setPassword(password);

		session.connect();

		return session;
	}

	public void go(Runnable ... runnable) {
		new Thread(() -> {
			for (Runnable r : runnable) {
				r.run();
			}
		}).start();
	}

	private static OutputStream loggerOutputStream() {

		return new OutputStream() {
			StringBuilder b = new StringBuilder();

			@Override
			public void write(int i) throws IOException {
				char c = (char) i;
				if (c == '\n') {
					Logger.info(b.toString());
					b = new StringBuilder();
				} else {
					b.append(c);
				}
			}
		};
	}

	public void execute(String command) throws Exception {

		Logger.info("Ssh Execute: " + command);

		Session session = session();

		Channel channel=session.openChannel("exec");
		((ChannelExec)channel).setCommand(command);


		channel.setInputStream(null);

		try(OutputStream out = loggerOutputStream()) {

			channel.setOutputStream(out);
			((ChannelExec) channel).setErrStream(out);

			InputStream in = channel.getInputStream();

			channel.connect();

			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0) break;
					Logger.info(new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					if (in.available() > 0) continue;
					Logger.info("exit-status: " + channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception ee) {
				}
			}
			channel.disconnect();
			session.disconnect();
		}
	}

	public void scpRemote(String from, String to) throws JSchException, IOException {
		FileOutputStream fos=null;

		Session session = session();

		String prefix=null;
		if(new File(to).isDirectory()){
			prefix=to+File.separator;
		}

		// exec 'scp -f rfile' remotely
		from = from.replace("'", "'\"'\"'");
		from = "'" + from + "'";
		String command="scp -f " + from;
		Channel channel=session.openChannel("exec");
		((ChannelExec)channel).setCommand(command);

		// get I/O streams for remote scp
		OutputStream out=channel.getOutputStream();
		InputStream in=channel.getInputStream();

		channel.connect();

		byte[] buf=new byte[1024];

		// send '\0'
		buf[0]=0; out.write(buf, 0, 1); out.flush();

		while(true){
			int c=checkAck(in);
			if(c!='C'){
				break;
			}

			// read '0644 '
			in.read(buf, 0, 5);

			long filesize=0L;
			while(true){
				if(in.read(buf, 0, 1)<0){
					// error
					break;
				}
				if(buf[0]==' ')break;
				filesize=filesize*10L+(long)(buf[0]-'0');
			}

			String file=null;
			for(int i=0;;i++){
				in.read(buf, i, 1);
				if(buf[i]==(byte)0x0a){
					file=new String(buf, 0, i);
					break;
				}
			}

			//System.out.println("filesize="+filesize+", file="+file);

			// send '\0'
			buf[0]=0; out.write(buf, 0, 1); out.flush();

			// read a content of lfile
			fos=new FileOutputStream(prefix==null ? to : prefix+file);
			int foo;
			while(true){
				if(buf.length<filesize) foo=buf.length;
				else foo=(int)filesize;
				foo=in.read(buf, 0, foo);
				if(foo<0){
					// error
					break;
				}
				fos.write(buf, 0, foo);
				filesize-=foo;
				if(filesize==0L) break;
			}
			fos.close();
			fos=null;

			if(checkAck(in)!=0){
				return;
			}

			// send '\0'
			buf[0]=0; out.write(buf, 0, 1); out.flush();
		}

		session.disconnect();

		return;
	}

	int checkAck(InputStream in) throws IOException{
		int b=in.read();
		// b may be 0 for success,
		//          1 for error,
		//          2 for fatal error,
		//          -1
		if(b==0) return b;
		if(b==-1) return b;

		if(b==1 || b==2){
			StringBuffer sb=new StringBuffer();
			int c;
			do {
				c=in.read();
				sb.append((char)c);
			}
			while(c!='\n');
			if(b==1){ // error
				System.out.print(sb.toString());
			}
			if(b==2){ // fatal error
				System.out.print(sb.toString());
			}
		}
		return b;
	}

	public void scp(String from, String to) throws Exception {

		FileInputStream fis=null;

		boolean ptimestamp = true;

		// exec 'scp -t rfile' remotely
//		rfile=rfile.replace("'", "'\"'\"'");
//		rfile="'"+rfile+"'";

		Session session = session();

		String command="scp " + (ptimestamp ? "-p" :"") +" -t " + to;
		Channel channel=session.openChannel("exec");
		((ChannelExec)channel).setCommand(command);

		// get I/O streams for remote scp
		OutputStream out=channel.getOutputStream();
		InputStream in=channel.getInputStream();

		channel.connect();

		if(checkAck(in)!=0){
			return;
		}

		File _lfile = new File(from);
		long filesize=_lfile.length();
		command="C0644 "+filesize+" ";
		if(from.lastIndexOf('/')>0){
			command += from.substring(from.lastIndexOf('/')+1);
		}
		else{
			command+=from;
		}
		command+="\n";
		out.write(command.getBytes()); out.flush();
		if(checkAck(in)!=0){
			return;
		}

		// send a content of lfile
		fis=new FileInputStream(from);
		byte[] buf=new byte[1024];
		while(true){
			int len=fis.read(buf, 0, buf.length);
			if(len<=0) break;
			out.write(buf, 0, len); //out.flush();
		}
		fis.close();
		fis=null;
		// send '\0'
		buf[0]=0; out.write(buf, 0, 1); out.flush();
		if(checkAck(in)!=0){
			System.exit(0);
		}
		out.close();

		channel.disconnect();
		session.disconnect();

	}

	public void scpDirectory(String directoryPath, String to) throws Exception {

		File directory = new File(directoryPath);

		execute("mkdir -p " + to);

		for (File f : directory.listFiles()) {
			if (f.isDirectory()) {
				scpDirectory(f.getAbsolutePath(), to + "/" + f.getName());
			} else {
				scp(f.getAbsolutePath(), to + "/");
			}
		}
	}

	public void scpContent(String content, String to) throws Exception {

		String tmpFileName = to.substring(to.lastIndexOf("/")) + randomString();

		File file = File.createTempFile(tmpFileName, "tmp");

		Util.spit(file.getAbsolutePath(), content);

		scp(file.getAbsolutePath(), to);

		rm(file.getAbsolutePath());
	}

	public String slurp(InputStream inputStream) throws Exception {
		StringWriter sw = new StringWriter();
		IOUtils.copy(inputStream, sw);
		return sw.toString();
	}

	@Override
	public String slurp(String path) {

		try {
			ChannelSftp sftp = (ChannelSftp) session().openChannel("sftp");
			sftp.connect();
			try (InputStream is = sftp.get(path)) {
				return slurp(is);
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public void spit(String path, String content) throws Exception {
		Session session = session();

		Channel channel = session.openChannel("sftp");
		channel.connect();

		ChannelSftp sftpChannel = (ChannelSftp) channel;

		// this will read file with the name test.txt and write to remote directory

		InputStream in = new ByteArrayInputStream(content.getBytes());

		sftpChannel.put(in, path);

		sftpChannel.exit();
		session.disconnect();
	}
}
