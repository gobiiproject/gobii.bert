package org.gobii.bert.shell;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import org.apache.commons.io.IOUtils;
import org.gobii.bert.ComponentsUtil;
import org.gobii.bert.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Local implements Shell {

	private static Logger logger = LoggerFactory.getLogger("Shell");

	@Override
	public void execute(String command) throws Exception {
		System.out.println("Local Execute: " + command);
		Process process = Runtime.getRuntime().exec(command);

		String results = Util.slurp(process.getInputStream());
		String errors = Util.slurp(process.getErrorStream());

		if (! results.trim().isEmpty()) {
			System.out.println("*** RESULTS ***\n" + results);
		}
		if (! errors.trim().isEmpty()) {
			System.out.println("*** ERRORS ***\n" + errors);
		}
	}

	@Override
	public void scpRemote(String from, String to) throws Exception {
		System.out.println(String.format("SCP: %s -> %s", from, to));
		Runtime.getRuntime().exec(String.format("cp %s %s", from, to));
	}

	@Override
	public void scp(String from, String to) throws Exception {
		System.out.println(String.format("SCP: %s -> %s", from, to));
		Runtime.getRuntime().exec(String.format("cp %s %s", from, to));
	}

	@Override
	public void scpDirectory(String directoryPath, String to) throws Exception {
		System.out.println(String.format("SCP: %s -> %s", directoryPath, to));
		Runtime.getRuntime().exec(String.format("cp -r %s %s", directoryPath, to));
	}

	@Override
	public void scpContent(String content, String to) throws Exception {
		System.out.println(String.format("SCP: %s -> %s", "###########", to));
		Util.spit(to, content);
	}

	public String slurp(InputStream inputStream) throws Exception {
		StringWriter sw = new StringWriter();
		IOUtils.copy(inputStream, sw);
		return sw.toString();
	}

	@Override
	public String slurp(String path) {
		try {
			FileInputStream inputStream = new FileInputStream(path);
			return slurp(inputStream);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void spit(String path, String content) throws Exception {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(content);
		writer.flush();
		writer.close();
	}
}
