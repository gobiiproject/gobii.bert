package org.gobii.bert;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;

public class Util {

	public static void spit(String path, String content) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(path));
		writer.write(content);
		writer.flush();
		writer.close();
	}

	public static String slurp(InputStream inputStream) throws IOException {
		StringWriter sw = new StringWriter();
		IOUtils.copy(inputStream, sw);
		return sw.toString();
	}

	public static String slurp(String path) throws IOException {
		FileInputStream inputStream = new FileInputStream(path);
		return slurp(inputStream);
	}

	public static <T> Map<T, T> into(Map<T, T> map, T ... ts) {
		if (! (ts.length % 2 == 0)) {
			throw new RuntimeException("An uneven number of key/values provided to Util.map");
		}

		for (int i = 0 ; i < ts.length ; i++) {
			map.put(ts[i], ts[i+1]);
		}

		return map;
	}

	public static <T> Map<T,T> map(T ... ts) {
		return into(new HashMap<>(), ts);
	}

	public static <S,T> Map<S, T> zipMap(List<S> keys, List<T> vals) {
		if (keys.size() != vals.size()) {
			throw new RuntimeException("Size of keys and values must be the same");
		}

		Map<S, T> map = new HashMap<>(keys.size());

		for (int i = 0 ; i < keys.size() ; i++) {
			map.put(keys.get(i), vals.get(i));
		}

		return map;
	}

	public static <T> List<T> listOf(T ... ts) {
		List<T> list = new ArrayList<>();

		for (T t : ts) {
			list.add(t);
		}

		return list;
	}
}
