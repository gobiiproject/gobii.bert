package org.gobii.bert;

import ernie.core.Ernie;
import java.io.File;
import org.gobii.bert.util.Assertions;

public class Bert {

	static String bertAndErnie =

			"                       \\WWW/      \n" +
			"                       /   \\       \n" +
			"                      /wwwww\\       \n" +
			"                    _|  o_o  |_     \n" +
			"       \\WWWWWWW/   (_   / \\   _)    \n" +
			"     _.'` o_o `'._   |  \\_/  |      \n" +
			"    (_    (_)    _)  : ~~~~~ :      \n" +
			"      '.'-...-'.'     \\_____/       \n" +
			"       (`'---'`)      [     ]       \n" +
			"        `\"\"\"\"\"`       `\"\"\"\"\"`\n";


	private static Ernie ernie = new Ernie();

	public static Ernie getErnie() {
		return ernie;
	}

	public static void main(String[] args) throws Exception {

		System.out.println(bertAndErnie);

		ernie.loadComponents(Components.class);
		ernie.loadComponents(BertUtils.class);
		ernie.loadComponents(Assertions.class);

		for (int i = 0 ; i < args.length ; i++) {
			if ("-c".equals(args[i])) {
				i++;
				ernie.runScript(args[i]);
			} else {
				System.out.println("Running file " + args[i]);
				ernie.runFile(args[i]);
			}
		}

		File dir = new File("./bert-results");
		if (! (dir.isDirectory() && dir.exists())) {
			dir.delete();
			dir.mkdir();
		}


		ernie.report("./bert-results");

//		Scanner scanner = new Scanner(System.in);
//		while (true) {
//
//			String line = scanner.nextLine();
//			if ("exit".equals(line)) {
//				break;
//			} else {
//				try {
//					System.out.println(ernie.runScript(line));
//				} catch (Throwable e) {
//					e.printStackTrace();
//				}
//			}
//		}

		System.exit(0);
	}
}
