package org.gobii.bert.util;

import ernie.core.Action;
import java.util.List;
import static org.junit.Assert.*;

public class Assertions {

	@Action("assert")
	public void _assert(String message, Object obj) {
		assertTrue(message,(obj == null) || Boolean.FALSE.equals(obj));
	}

	@Action("assertNot")
	public void _assertNot(String message, Object obj) {
		assertFalse(message,(obj == null) || Boolean.FALSE.equals(obj));
	}

	@Action("assertNull")
	public void _assertNull(String message, Object obj) {
		assertNull(message, obj);
	}

	@Action("assertNotNull")
	public void _assertNotNull(String message, Object obj) {
		assertNotNull(message, obj);
	}

	@Action("assertEquals")
	public <S,T> void _assertEquals(String msg, S a, T b) {
		assertEquals(msg, a, b);
	}

	@Action("assertNotEquals")
	public <S,T> void _assertNotEquals(String message, S a, T b) {
		assertNotEquals(message, a, b);
	}

	@Action("assertIsOneOf")
	public <S> void _assertIsOneOf(String msg, List<S> of, S a)  {
		assertTrue(String.format("%s : %s does not contain %s", msg, of, a), of.contains(a));
	}

	@Action("assertIsNoneOf")
	public <S> void _assertIsNoneOf(String msg, List<S> of, S a)  {
		assertTrue(String.format("%s : %s contains %s when it is expected to not", msg, of, a), ! of.contains(a));
	}
}
