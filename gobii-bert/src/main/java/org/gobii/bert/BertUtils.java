package org.gobii.bert;

import ernie.core.Action;
import ernie.core.Clean;
import ernie.core.Verify;
import java.io.BufferedReader;
import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Stack;
import java.util.stream.Collectors;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static junit.framework.TestCase.assertTrue;

public class BertUtils {


	private Icon icon;
	{
		try {
			icon = new ImageIcon(getClass().getClassLoader().getResource("bert.jpg"));
		} catch (Exception e) {

		}
	}

	private static List<String> words;

	static {
		try (InputStream in = BertUtils.class.getClassLoader().getResource("words.txt").openConnection().getInputStream()) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			words = reader.lines().collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Action("prompt")
	public String prompt(String message) {
		System.out.print(message);
		System.out.flush();
		return new Scanner(System.in).next();
	}

	@Action("promptWithGui")
	public String promptWithGui(String message) {
		return (String) JOptionPane.showInputDialog(new JFrame(), message, "Bert", 0, icon, null, null);
	}

	@Action("notify")
	public void notify(String msg) {
		JOptionPane.showMessageDialog(new JFrame(), msg, "Bert", 0, icon);
	}

	@Action("genstr")
	public String genstr(String base) {
		return base + words.get(new Random().nextInt(words.size()));
	}

	private Stack<String> pwd = new Stack<>();

	@Action("withDirectory")
	@Action("cd")
	public void cd(String path) {
		if ("-".equals(path)) {
			pwd.pop();
		}
		else if (path.startsWith(File.separator)) {
			pwd.clear();
			pwd.push(path);
		} else if (path.startsWith("." + File.separator)) {
			path = path.substring(2);
			pwd.push(path);
		} else if ("..".equals(path)) {
			pwd.pop();
		} else if (path.startsWith(".." + File.separator)) {
			pwd.pop();
			cd(path.substring(3));
		} else {
			if (path.endsWith(File.separator)) {
				pwd.push(path);
			} else {
				pwd.push(path + File.separator);
			}
		}
	}

	@Verify("withDirectory")
	@Verify("cd")
	public void verifyCd() {
		assertTrue(String.format("%s does not exist", pwd()), new File(pwd()).exists());
	}

	@Clean("withDirectory")
	public void cleanWithDirectory(String path) {
		pwd.pop();
	}

	@Action("pwd")
	public String pwd() {
		String result = pwd.stream()
				.collect(Collectors.joining("/"));
		return result;
	}

	@Action("filePath")
	public String filePath(String path) {

		if (path == null) {
			return null;
		} else if (path.startsWith("/")) {
			return path;
		}
		return pwd() + "/" + path;
	}

	@Action("sh")
	public String sh(String command) throws IOException {
		Process process = Runtime.getRuntime().exec(String.format("(cd %s && %s)", pwd(), command));
		return Util.slurp(process.getInputStream());
	}

	@Action("runErnieFile")
	public Object ernie(String path) {
		return Bert.getErnie().runFile(path);
	}

	@Action("runErnieFiles")
	public void runErnieFiles(List<String> files) {
		for (String file : files) {
			ernie(file);
		}
	}

	@Action("runErnieScript")
	public Object runErnieScript(String script) {
		return Bert.getErnie().runScript(script);
	}

	private static int breadthFirstSearchFileComparator(File f0, File f1) {
		if (f0.isDirectory() && f1.isDirectory()) {
			return 0;
		} else if (f0.isDirectory()){
			return 1;
		} else if (f1.isDirectory()) {
			return -1;
		} else {
			return 0;
		}
	}

	@Action("find")
	public List<String> find(String path, String regex) {
		File root = new File(path);
		File[] fileArr = root.listFiles();
		LinkedList<File> files = fileArr != null ? new LinkedList<>(Arrays.asList(fileArr))
												 : new LinkedList<>();

		files.sort(BertUtils::breadthFirstSearchFileComparator); // Enforce breadth first search

		try {
			files.sort(Comparator.comparing(File::getName));
		} catch (Exception e) {}

		LinkedList<String> names = new LinkedList<>();

		for (File f : files) {
			if (f.isDirectory()) {
				names.addAll(find(f.getAbsolutePath(), regex));
			} else if (f.getName().matches(regex)){
				names.add(f.getAbsolutePath());
			}
		}


		return names;
	}

	@Action("pause")
	public void pause() {
		Console c = System.console();
		if (c != null) {
			c.format("\nPress ENTER to proceed.\n");
			c.readLine();

		}
	}

	@Action("pause")
	public void pause(Long n) throws Exception {
		Thread.sleep(n);
	}

	private Map<String, Object> environment = new HashMap<>();

	@Action("environment/get")
	public Object get(String key) {
		return environment.get(key);
	}

	@Action("environment/set")
	public void set(String key, Object value) {
		this.environment.put(key, value);
	}

}
