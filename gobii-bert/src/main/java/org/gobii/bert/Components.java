package org.gobii.bert;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import ernie.core.Action;
import ernie.core.Clean;
import ernie.core.Verify;
import ernie.core.Logger;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.gobii.bert.matrix.Vector;
import org.gobii.bert.matrix.Matrix;
import org.gobii.bert.shell.Local;
import org.gobii.bert.shell.Shell;
import org.gobii.bert.shell.Ssh;
import org.gobiiproject.gobiiapimodel.payload.PayloadEnvelope;
import org.gobiiproject.gobiimodel.cvnames.JobProgressStatusType;
import org.gobiiproject.gobiimodel.dto.auditable.*;
import org.gobiiproject.gobiimodel.dto.children.PropNameId;
import org.gobiiproject.gobiimodel.dto.children.VendorProtocolDTO;
import org.gobiiproject.gobiimodel.dto.instructions.extractor.ExtractorInstructionFilesDTO;
import org.gobiiproject.gobiimodel.dto.instructions.extractor.GobiiDataSetExtract;
import org.gobiiproject.gobiimodel.dto.instructions.extractor.GobiiExtractorInstruction;
import org.gobiiproject.gobiimodel.dto.instructions.loader.*;
import org.gobiiproject.gobiimodel.dto.noaudit.CvDTO;
import org.gobiiproject.gobiimodel.dto.noaudit.CvGroupDTO;
import org.gobiiproject.gobiimodel.dto.noaudit.DataSetDTO;
import org.gobiiproject.gobiimodel.dto.noaudit.JobDTO;
import org.gobiiproject.gobiimodel.dto.system.AuthDTO;
import org.gobiiproject.gobiimodel.dto.system.PingDTO;
import org.gobiiproject.gobiimodel.types.GobiiColumnType;
import org.gobiiproject.gobiimodel.types.GobiiCvGroupType;
import org.gobiiproject.gobiimodel.types.GobiiProcessType;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.File;
import java.io.IOException;
import java.util.Date;



import static java.lang.String.format;

import static org.gobii.bert.ComponentsUtil.*;

import static org.gobii.bert.Util.slurp;
import static org.junit.Assert.*;

public class Components {

	private static final String HEADER_USERNAME = "X-Username";
	private static final String HEADER_PASSWORD = "X-Password";
	private static final String HEADER_AUTH_TOKEN = "X-Auth-Token";

	private static final String URL_BASE = "%s:%s%s";

	private static final String URL_AUTH = "/auth";
	private static final String URL_PING = "/ping";
	private static final String URL_CONTACTS = "/contacts";
	private static final String URL_CONTACT = "/contacts/%s";
	private static final String URL_CONTACT_SEARCH = "/contact-search";
	private static final String URL_PROJECT = "/projects";
	private static final String URL_EXPERIMENTS = "/experiments";
	private static final String URL_PROTOCOLS = "/protocols";
	private static final String URL_PLATFORMS = "/platforms";
	private static final String URL_VENDOR_PROTOCOL = "/protocols/%s/vendors";
	private static final String URL_ORGANIZATIONS = "/organizations";
	private static final String URL_JOBS = "/jobs/%s";
	private static final String URL_MAPSETS = "/mapsets";
	private static final String URL_ANALYSES = "/analyses";
	private static final String URL_DATASETS = "/datasets";
	private static final String URL_CVS = "/cvs";
	private static final String URL_CV_GROUP = "/cvgroups/%s";
	private static final String URL_INSTRUCTIONS_EXTRACTOR = "/instructions/extractor";
	private static final String URL_INSTRUCTIONS_LOADER = "/instructions/loader";

	private Database database;

	private String host;
	private Integer port;
	private String path;
	private String authToken;
	private String user;
	private String password;
	private String crop;

	private String baseFileDirectory = "";

	private Shell shell;

	private Set<Integer> contactsToNotDelete = new HashSet<>();

	private String url(String endpoint, Object ... params) {
		return "http://" + format(format(URL_BASE, host, port, path) + endpoint, params);
	}

	private String database() {
		return "gobii_" + crop;
	}

	private HttpHeaders authHeader() {
		return ComponentsUtil.headers(HEADER_AUTH_TOKEN, authToken, HEADER_USERNAME, user, HEADER_PASSWORD, password);
	}

	@Action("baseFileDirectory")
	public String setBaseFileDirectory(String path) {
		this.baseFileDirectory = path;
		return this.baseFileDirectory;
	}

	@Action("baseFileDirectory")
	public String getBaseFileDirectory() {
		return baseFileDirectory;
	}

	@Action("database")
	public Database database(String dbHost, Long dbPort, String dbName, String dbUser, String dbPassword) throws SQLException {
		database = new Database();
		database.connect(dbHost, dbPort.intValue(), dbName, dbUser, dbPassword);
		return database;
	}

	@Action("crop")
	public String crop(String crop) {
		this.crop = crop;

		return crop;
	}

	@Verify("crop")
	public void crop(String r, String crop) {
		assertNotNull(this.crop);
	}

	@Action("shell")
	public void shell(String host, String user, String password) throws Exception {
		if ("localhost".equals(host)) {
			this.shell = new Local();
		} else {
			this.shell = new Ssh(host, user, password);
		}
	}

	@Action("host")
	public ResponseEntity<AuthDTO> host(String host, Long port, String path, String user, String password) {
		this.host = host;
		this.port = port.intValue();
		this.path = path;

		ResponseEntity<AuthDTO> response;

		try {
			response = ComponentsUtil.post(url(URL_AUTH),
					ComponentsUtil.headers(HEADER_USERNAME, user,
							HEADER_PASSWORD, password),
					AuthDTO.class);
		} catch (HttpServerErrorException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}

		return response;
	}

	@Verify("host")
	public void verifyHost(ResponseEntity<AuthDTO> response, String host, Long port, String path, String user, String password) {
		String authToken = response.getHeaders().get(HEADER_AUTH_TOKEN).get(0);

		ResponseEntity<PingDTO> pingResponse = ComponentsUtil.post(url(URL_PING), ComponentsUtil.headers(HEADER_AUTH_TOKEN, authToken),
													 PingDTO.class, new PingDTO());


		assertEquals("AUTH Response", HttpStatus.OK, response.getStatusCode());
		assertEquals("PING Response", HttpStatus.OK, pingResponse.getStatusCode());

		this.authToken = authToken;
		this.user = user;
		this.password = password;
	}

	@Action("contactSearch")
	public ContactDTO contactSearch(String email) {

		try {
			String url = url(URL_CONTACT_SEARCH);
			ResponseEntity<JsonNode> response = ComponentsUtil.get(url, authHeader(), JsonNode.class,
					"email", email,
					"userName", "",
					"lastName", "",
					"firstName", "");
			return ComponentsUtil.result(response, ContactDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Action("contact")
	public ContactDTO contact(String username, String firstname, String lastname, String email) {

		ContactDTO contact = contactSearch(email);

		boolean contactExists = (contact != null);

		if (contactExists) {
			contactsToNotDelete.add(contact.getId());
		} else {
			contact = new ContactDTO();
		}

		contact.setCode("code");
		contact.setUserName(username);
		contact.setFirstName(firstname);
		contact.setLastName(lastname);
		contact.setEmail(email);
		contact.setCreatedBy(1);
		contact.setModifiedBy(1);
		contact.setRoles(new ArrayList<>());
		contact.getRoles().add(5);
		contact.getRoles().add(3);
		contact.getRoles().add(1);

		if (! contactExists) {
			PayloadEnvelope<ContactDTO> envelope = new PayloadEnvelope<>(contact, GobiiProcessType.CREATE);

			try {
				ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_CONTACTS), authHeader(), JsonNode.class, envelope);
				return ComponentsUtil.result(response, ContactDTO.class);
			} catch (HttpStatusCodeException e) {
				ComponentsUtil.printHttpError(e);
				throw e;
			}
		} else {
			return contact;
		}
	}

	@Clean("contact")
	public void cleanContact(ContactDTO contact) throws SQLException {
		if (! contactsToNotDelete.contains(contact.getContactId())) {
			database.delete(contact);
		}
	}

	@Action("project")
	public ProjectDTO createProject(ContactDTO pi, String name) throws Exception {

		ProjectDTO project = new ProjectDTO();
		project.setCreatedBy(1);
		project.setProjectName(name);
		project.setProjectCode("_" + name);
		project.setModifiedBy(1);
		project.setCreatedBy(1);
		project.setProjectStatus(1);
		project.setPiContact(pi.getContactId());

		PayloadEnvelope<ProjectDTO> envelope = new PayloadEnvelope<>(project, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_PROJECT), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, ProjectDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("project")
	public void cleanProject(ProjectDTO project) throws SQLException {
		database.delete(project);
	}

	@Action("protocol")
	public ProtocolDTO createProtocol(PlatformDTO platform, String name) throws Exception {

		ProtocolDTO protocol = new ProtocolDTO();

		protocol.setName(name);
		protocol.setPlatformId(platform.getId());
		protocol.setModifiedBy(1);
		protocol.setCreatedBy(1);
		protocol.setTypeId(1);
		protocol.setStatus(1);

		PayloadEnvelope<ProtocolDTO> envelope = new PayloadEnvelope<>(protocol, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_PROTOCOLS), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, ProtocolDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("protocol")
	public void cleanProtocol(ProtocolDTO protocol) throws SQLException {
		database.delete(protocol);
	}

	@Action("platform")
	public PlatformDTO createPlatform(String name) throws Exception {

		PlatformDTO platform = new PlatformDTO();
		platform.setPlatformName(name);
		platform.setPlatformCode(name);
		platform.setModifiedBy(1);
		platform.setCreatedBy(1);
		platform.setStatusId(1);
		platform.setTypeId(1);

		PayloadEnvelope<PlatformDTO> envelope = new PayloadEnvelope<>(platform, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_PLATFORMS), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, PlatformDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("platform")
	public void cleanPlatform(PlatformDTO platform) throws SQLException {
		database.delete(platform);
	}

	@Action("experiment")
	public ExperimentDTO createExperiment(ProjectDTO project, VendorProtocolDTO protocol, String name) throws Exception {

		ExperimentDTO experiment = new ExperimentDTO();
		experiment.setExperimentName(name);
		experiment.setExperimentCode(name);
		experiment.setVendorProtocolId(protocol.getId());
		experiment.setProjectId(project.getId());
		experiment.setModifiedBy(1);
		experiment.setCreatedBy(1);
		experiment.setStatusId(1);

		PayloadEnvelope<ExperimentDTO> envelope = new PayloadEnvelope<>(experiment, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_EXPERIMENTS), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, ExperimentDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("experiment")
	public void cleanExperiment(ExperimentDTO experiment) throws SQLException {
		database.delete(experiment);
	}

	@Action("organization")
	public OrganizationDTO organization(String name) throws Exception{
		OrganizationDTO organization = new OrganizationDTO();

		organization.setName(name);
		organization.setStatusId(1);
		organization.setAddress("");
		organization.setWebsite("");
		organization.setCreatedBy(1);
		organization.setModifiedBy(1);

		PayloadEnvelope<OrganizationDTO> envelope = new PayloadEnvelope<>(organization, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_ORGANIZATIONS), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, OrganizationDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("organization")
	public void cleanOrganization(OrganizationDTO organization) throws SQLException {
		database.delete(organization);
	}

	@Action("vendorProtocol")
	public VendorProtocolDTO vendorProtocol(OrganizationDTO vendor, ProtocolDTO protocol) throws Exception {

		try {
			PayloadEnvelope<OrganizationDTO> envelope = new PayloadEnvelope<>(vendor, GobiiProcessType.CREATE);
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_VENDOR_PROTOCOL, protocol.getId()), authHeader(), JsonNode.class, envelope);
			OrganizationDTO updatedVendor = ComponentsUtil.result(response, OrganizationDTO.class);
			
			return updatedVendor.getVendorProtocols().stream()
					.filter(vp -> vp.getProtocolId().equals(protocol.getId()))
					.findFirst().get();
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("vendorProtocol")
	public void cleanVendorProtocol(VendorProtocolDTO protocol) throws SQLException {
		database.delete(protocol);
	}

	@Action("mapset")
	public MapsetDTO mapset(String name) {
		MapsetDTO mapset = new MapsetDTO();

		mapset.setName(name);
		mapset.setDescription(name);
		mapset.setCode("");
		mapset.setMapType(1);
		mapset.setModifiedBy(1);
		mapset.setCreatedBy(1);
		mapset.setStatusId(1);

		PayloadEnvelope<MapsetDTO> envelope = new PayloadEnvelope<>(mapset, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_MAPSETS), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, MapsetDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("mapset")
	public void cleanMapset(MapsetDTO mapset) throws SQLException {
		database.delete(mapset);
	}

	@Action("cv")
	public CvDTO cv(CvGroupDTO group, String term, String definition) {

		CvDTO cv = new CvDTO();

		cv.setTerm(term);
		cv.setDefinition(definition);
		cv.setGroupId(group.getCvGroupId());
		cv.setRank(1);
		cv.setEntityStatus(1);

		PayloadEnvelope<CvDTO> envelope = new PayloadEnvelope<>(cv, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_CVS), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, CvDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("cv")
	public void cleanCv(CvDTO cv) throws SQLException {
		database.delete(cv);
	}

	@Action("readCv")
	public CvDTO readCv(String term) {
		try {
			ResponseEntity<JsonNode> allCvs = ComponentsUtil.get(url(URL_CVS), authHeader(), JsonNode.class);
			JsonNode json = allCvs.getBody().get("payload").get("data");

			for (JsonNode j : json) {
				if (term.equalsIgnoreCase(j.get("term").asText())) {
					return new ObjectMapper().convertValue(j, CvDTO.class);
				}
			}

			return null;
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Action("cvGroup")
	public CvGroupDTO cvGroup(String name) {
		try {
			ResponseEntity<JsonNode> cvGroup = ComponentsUtil.get(url(URL_CV_GROUP, name), authHeader(),
					JsonNode.class, "cvGroupTypeId",
					GobiiCvGroupType.GROUP_TYPE_SYSTEM.getGroupTypeId().toString());
			return ComponentsUtil.result(cvGroup, CvGroupDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Action("analysis")
	public AnalysisDTO analysis(CvDTO cv, String name) {

		AnalysisDTO analysis = new AnalysisDTO();

		analysis.setAnalysisName(name);
		analysis.setAnlaysisTypeId(cv.getCvId());
		analysis.setAnalysisDescription(name);
		analysis.setProgram(name);
		analysis.setProgramVersion(name);
		analysis.setAlgorithm(name);
		analysis.setSourceName(name);
		analysis.setSourceUri(name);
		analysis.setSourceName(name);
		analysis.setSourceVersion(name);
		analysis.setStatusId(1);
		analysis.setCreatedBy(1);
		analysis.setModifiedBy(1);

		PayloadEnvelope<AnalysisDTO> envelope = new PayloadEnvelope<>(analysis, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_ANALYSES), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, AnalysisDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("analysis")
	public void cleanAnalysis(AnalysisDTO analysis) throws SQLException {
		database.delete(analysis);
	}

	@Action("dataset")
	public DataSetDTO dataset(GobiiLoaderProcedure procedure, ExperimentDTO experiment, AnalysisDTO analysis, String name) {
		DataSetDTO dataset = new DataSetDTO();
		dataset.setDatasetName(name);
		dataset.setExperimentId(experiment.getExperimentId());
		dataset.setCallingAnalysisId(analysis.getId());
		dataset.setStatusId(1);
		dataset.setCreatedBy(1);
		dataset.setModifiedBy(1);

		if (procedure.getMetadata().getDatasetType().getId() != null) {
			dataset.setDatatypeId(procedure.getMetadata().getDatasetType().getId());
		} else {
			CvDTO cv = readCv(procedure.getMetadata().getDatasetType().getName());
			dataset.setDatatypeId(cv.getId());
		}

		PayloadEnvelope<DataSetDTO> envelope = new PayloadEnvelope<>(dataset, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_DATASETS), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, DataSetDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Clean("dataset")
	public void cleanDataset(DataSetDTO dataset) throws SQLException {
		database.delete(dataset);
	}

	@Action("job")
	public JobDTO job(DataSetDTO dataset, String jobType, String jobPayloadType, String name) {
		try {
			JobDTO job = new JobDTO();
			job.setSubmittedBy(1);
			job.setMessage("");
			job.setPayloadType(jobPayloadType);
			job.setType(jobType);
			job.setStatus(JobProgressStatusType.CV_PROGRESSSTATUS_PENDING.getCvName());
			job.setSubmittedDate(new Date());
			job.setJobName(name);

			if (dataset != null) {
				job.getDatasetIds().add(dataset.getDataSetId());
			}

			PayloadEnvelope<JobDTO> envelope = new PayloadEnvelope<>(job, GobiiProcessType.CREATE);
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_JOBS), authHeader(), JsonNode.class, envelope);

			return ComponentsUtil.result(response, JobDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Action("readJob")
	public JobDTO readJob(String name) {
		try {
			ResponseEntity<JsonNode> job = ComponentsUtil.get(url(URL_JOBS, name), authHeader(), JsonNode.class);
			JobDTO j = ComponentsUtil.result(job, JobDTO.class);
			return j;
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}
	}

	@Action("parseLoaderProcedure")
	public GobiiLoaderProcedure parseLoaderInstruction(String content) throws IOException {
		return new ObjectMapper().readValue(content, GobiiLoaderProcedure.class);
	}

	@Action("loaderProcedure")
	public LoaderInstructionFilesDTO loaderProcedure(LoaderInstructionFilesDTO procedure) {
		PayloadEnvelope<LoaderInstructionFilesDTO> envelope = new PayloadEnvelope<>(procedure, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_INSTRUCTIONS_LOADER), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, LoaderInstructionFilesDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}

	}

	@Action("load")
	public LoaderInstructionFilesDTO load(ContactDTO pi, ProjectDTO project, ExperimentDTO experiment, PlatformDTO platform,
									 	  DataSetDTO dataset, MapsetDTO mapset, String procedureFilePath, String dataFilePath) throws Exception {

		Logger.info("Loading " + procedureFilePath);

		GobiiLoaderProcedure procedure = new ObjectMapper().readValue(slurp(procedureFilePath), GobiiLoaderProcedure.class);

		if (procedure == null) {
			throw new Exception("Procedure is null, for path " + procedureFilePath);
		}

		fillInProcedurePrototype(procedure, pi, project, experiment, platform, dataset, mapset, crop);

		shell.execute(format("mkdir -p %s", procedure.getMetadata().getGobiiFile().getSource()));

		String data = slurp(dataFilePath);

		List<GobiiFileColumn> uuidColumns =
				procedure.getInstructions()
						.stream()
						.map(instruction ->
								instruction.getGobiiFileColumns()
										.stream()
										.filter(fc -> "uuid".equalsIgnoreCase(fc.getName()))
										.collect(Collectors.toList()))
						.reduce((a, b) -> {a.addAll(b); return a;})
						.get();

		if (uuidColumns != null && ! uuidColumns.isEmpty()) {
			Matrix<String> matrix = ComponentsUtil.tsvToMatrix(data);

			for (GobiiFileColumn fc : uuidColumns) {
				int row = fc.getrCoord();
				int col = fc.getcCoord();

				if (GobiiColumnType.CSV_COLUMN.equals(fc.getGobiiColumnType())) {
					for (int i = row; i < matrix.height(); i++) {
						matrix.set(i, col, UUID.randomUUID().toString());
					}
				} else if (GobiiColumnType.CSV_ROW.equals(fc.getGobiiColumnType())) {
					for (int i = col; i < matrix.width(); i++) {
						matrix.set(row, i, UUID.randomUUID().toString());
					}
				}
			}

			data = ComponentsUtil.matrixToTsv(matrix);
		}


		String dataFileName = dataFilePath.substring(dataFilePath.lastIndexOf('/'));
		shell.spit(procedure.getMetadata().getGobiiFile().getSource() + dataFileName, data);

		String procedureFileName = new File(procedureFilePath).getName();
		procedureFileName = procedureFileName.substring(0, procedureFileName.lastIndexOf(".json"));

		String procedureFileNewName = UUID.randomUUID().toString().replaceAll("-", "_");

		String procedureFileRemoteDirectory = format("/data/gobii_bundle/crops/%s/loader/inprogress/", crop);
		String procedureFileRemotePath = procedureFileRemoteDirectory + procedureFileNewName + ".json";

		LoaderInstructionFilesDTO dto = new LoaderInstructionFilesDTO();
		dto.setGobiiLoaderProcedure(procedure);
		dto.setInstructionFileName(procedureFileNewName);

		LoaderInstructionFilesDTO dto1 = loaderProcedure(dto);

		shell.execute(
				format("mv /data/gobii_bundle/crops/%s/loader/instructions/%s.json " +
								"/data/gobii_bundle/crops/%s/loader/inprogress/%s.json",
						crop, procedureFileNewName,
						crop, procedureFileNewName));

		shell.execute(format("(cd /data/gobii_bundle/core && java -Dorg.gobii.environment.name=BERT -jar Digester.jar %s)", procedureFileRemotePath));

		return dto;
	}

	@Verify("load")
	public void verifyLoad(LoaderInstructionFilesDTO dto, ContactDTO pi, ProjectDTO project, ExperimentDTO experiment,
							  PlatformDTO platform, DataSetDTO dataset, MapsetDTO mapset, String procedureFilePath,
							  String dataFilePath) throws Exception {


		String dataFileRemotePath = dto.getProcedure().getMetadata().getGobiiFile().getSource()
				+ dataFilePath.substring(dataFilePath.lastIndexOf("/"));

		String dataContents = shell.slurp(dataFileRemotePath);


		Matrix<String> wholeFileMatrix = ComponentsUtil.tsvToMatrix(dataContents);

		for (GobiiLoaderInstruction instruction : dto.getProcedure().getInstructions()) {

			if (instruction.getTable().matches(".*?_prop")) {
				continue;
			}

			// Strip off dataset_ from the beginning, or _prop from the end
			Matcher m = Pattern.compile("(dataset_)?(.*?)(_prop)?").matcher(instruction.getTable());
			m.matches();
			instruction.setTable(m.group(2));

			if (database.tableExists(instruction.getTable())) {

				Set<String> columnNames = database.getTableColumnNames(instruction.getTable());
				LinkedHashMap<String, String> constantValues = new LinkedHashMap<>();
				LinkedHashMap<String, Vector<String>> cardinals = new LinkedHashMap<>();

				for (GobiiFileColumn gfc : instruction.getGobiiFileColumns()) {

					if (gfc.getName().matches(String.format("%s_name", instruction.getTable()))) {
						gfc.setName("name");
					}

					if (columnNames.contains(gfc.getName()) || gfc.isSubcolumn()) {
						switch (gfc.getGobiiColumnType()) {
							case CONSTANT:
								constantValues.put(gfc.getName(), gfc.getConstantValue());
								break;
							case CSV_COLUMN:
								cardinals.put(gfc.getName(), wholeFileMatrix.getColumn(gfc.getcCoord()).slice(gfc.getrCoord()));
								break;
							case CSV_ROW:
								cardinals.put(gfc.getName(), wholeFileMatrix.getRow(gfc.getrCoord()).slice(gfc.getcCoord()));
								break;
						}
					}
				}

				if (constantValues.isEmpty()) {
					continue;
				}

				if (cardinals.isEmpty()) {
					continue;
				}

				List<Map<String, Object>> cardinalRows = new LinkedList<>();

				int cardinalsHeight = cardinals.values().iterator().next().length();

				for (int i = 0; i < cardinalsHeight; i++) {
					Map<String, Object> cardinalRow = new HashMap<>();
					for (String cardinalName : cardinals.keySet()) {
						cardinalRow.put(cardinalName, cardinals.get(cardinalName).get(i));
					}
					cardinalRows.add(cardinalRow);
				}

				Set<String> unusableConstants = new HashSet<>();
				unusableConstants.add("props");
				unusableConstants.add("status");

				LinkedList<String> predicates = new LinkedList<>();
				for (Map.Entry<String, String> constant : constantValues.entrySet()) {
					if (!unusableConstants.contains(constant.getKey())) {
						predicates.add(format("%s = %s", constant.getKey(), constant.getValue()));
					}
				}

				if (predicates.isEmpty()) {
					continue;
				}

				String query = format("SELECT %s FROM %s WHERE %s",
						String.join(",", cardinals.keySet()),
						instruction.getTable(), String.join(" AND ", predicates));
				ResultSet rs = database.query(query);

				List<Map<String, Object>> results = readResultSet(rs);

				Set<Map<String, Object>> rows = new HashSet<>(results);

				for (Map<String, Object> cardinalRow : cardinalRows) {
					if (cardinalRow.get("alts") != null) {
						String alts = (String) cardinalRow.get("alts");
						List<String> altsList;
						if (alts.isEmpty()) {
							altsList = new ArrayList<>();
						} else {
							alts = alts.replaceAll("\\[|]|\\{|}\\(|\\)", "");
							altsList = Arrays.stream(alts.split("[^0-9A-Za-z\\-+.]"))
							        .filter(x -> ! "".equals(x))
									.collect(Collectors.toList());
						}

						cardinalRow.put("alts", altsList);
					}

					if (cardinalRow.get("start") != null && ! cardinalRow.get("start").toString().isEmpty()) {

						cardinalRow.put("start", new BigDecimal(cardinalRow.get("start").toString()).setScale(3, RoundingMode.DOWN));
					}

					if (cardinalRow.get("stop") != null && ! cardinalRow.get("stop").toString().isEmpty()) {
						cardinalRow.put("stop", new BigDecimal(cardinalRow.get("stop").toString()).setScale(3, RoundingMode.DOWN));
					}

					if (! rows.contains(cardinalRow)) {
						fail("Row from datafile was not found in database: " + cardinalRow.toString());
					}
				}
			}
		}
	}

	@Clean("load")
	public void cleanLoad(LoaderInstructionFilesDTO dto) throws Exception {

		if (dto == null || dto.getProcedure() == null) {
			return;
		}

		GobiiLoaderProcedure procedure = dto.getProcedure();

		String procedureFileRemoteDirectory = format("/data/gobii_bundle/crops/%s/loader/done/", procedure.getMetadata().getGobiiCropType());
		String procedureFileRemotePath = procedureFileRemoteDirectory + dto.getInstructionFileName() + ".json";

		shell.execute("rm " + procedureFileRemotePath);
		shell.execute("rm -r " + procedure.getMetadata().getGobiiFile().getSource());

		database.sqlExecute(format("DELETE FROM dnarun WHERE experiment_id = %s",
						procedure.getMetadata().getExperiment().getId()));

		ResultSet rs = database.query(format("SELECT * FROM germplasm WHERE germplasm_id " +
						                     "IN (SELECT germplasm_id FROM dnasample WHERE project_id = %s)",
				procedure.getMetadata().getProject().getId()));

		Set<Integer> germplasmKeys = new HashSet<>();


		while (rs.next()) {
			germplasmKeys.add(rs.getInt("germplasm_id"));
		}

		rs.close();

		database.sqlExecute(format("DELETE FROM dnasample WHERE project_id = %s",
						procedure.getMetadata().getProject().getId()));

		if (! germplasmKeys.isEmpty()) {
			database.sqlExecute(format("DELETE FROM germplasm WHERE germplasm_id IN (%s)",
									   germplasmKeys.stream()
													.map(Object::toString)
													.collect(Collectors.joining(","))));
		}

		database.sqlExecute(format("DELETE FROM marker_linkage_group " +
										  "WHERE marker_id " +
				                          "IN (SELECT marker_id " +
				                              "FROM marker " +
				                              "WHERE platform_id = %s)",
				                          procedure.getMetadata().getPlatform().getId()));

		database.sqlExecute(format("DELETE FROM marker WHERE platform_id = %s",
						procedure.getMetadata().getPlatform().getId()));

		database.sqlExecute(format("DELETE FROM linkage_group WHERE map_id = %s",
				procedure.getMetadata().getMapset().getId()));

		database.sqlExecute(format("DELETE FROM dataset WHERE dataset_id = %s",
				procedure.getMetadata().getDataset().getId()));

		database.sqlExecute(format("DELETE FROM job WHERE submitted_by = %s",
				procedure.getMetadata().getContactId()));
	}

	@Action("extractorInstruction")
	public ExtractorInstructionFilesDTO extractorInstruction(ExtractorInstructionFilesDTO instructionsDto) {
		PayloadEnvelope<ExtractorInstructionFilesDTO> envelope = new PayloadEnvelope<>(instructionsDto, GobiiProcessType.CREATE);

		try {
			ResponseEntity<JsonNode> response = ComponentsUtil.post(url(URL_INSTRUCTIONS_EXTRACTOR), authHeader(), JsonNode.class, envelope);
			return ComponentsUtil.result(response, ExtractorInstructionFilesDTO.class);
		} catch (HttpStatusCodeException e) {
			ComponentsUtil.printHttpError(e);
			throw e;
		}

	}


	@Action("extract")
	public ExtractorInstructionFilesDTO extract(ContactDTO pi, ProjectDTO project, ExperimentDTO experiment, PlatformDTO platform,
												VendorProtocolDTO vendorProtocol, OrganizationDTO organization, ProtocolDTO protocol,
												AnalysisDTO analysis, DataSetDTO dataset, String instructionFilePath, List<String> filesToCompare)
			throws Exception {

		Logger.info(format("EXTRACT: %s", instructionFilePath));

		String fileName = new File(instructionFilePath).getName();

		List<GobiiExtractorInstruction> instructions = Arrays.asList(
				new ObjectMapper().readValue(slurp(instructionFilePath), GobiiExtractorInstruction[].class));


		for (GobiiExtractorInstruction instruction : instructions) {
			for (GobiiDataSetExtract extract : instruction.getDataSetExtracts()) {
				extract.setProject(new PropNameId(project.getId(), project.getProjectName()));
				extract.setDataSet(new PropNameId(dataset.getId(), dataset.getDatasetName()));
				extract.getPlatforms().clear();
				extract.getPlatforms().add(new PropNameId(platform.getId(), platform.getPlatformName()));
				extract.setPrincipleInvestigator(new PropNameId(pi.getId(), pi.getUserName()));
				extract.setExtractDestinationDirectory(
						format("/data/gobii_bundle/crops/%s/extractor/output/%s/%s/whole_dataset/%s",
								crop, pi.getEmail().substring(0, pi.getEmail().indexOf('@')), extract.getGobiiFileType().toString().toLowerCase(), fileName));
			}

			instruction.setGobiiCropType(crop);
			instruction.setContactId(pi.getId());
			instruction.setContactEmail(pi.getEmail());
		}

		String instructionsFileName = UUID.randomUUID().toString().replaceAll("-", "_");

		String instructionsFileRemoteDirectory = format("/data/gobii_bundle/crops/%s/extractor/inprogress/", crop);
		String instructionsFileRemotePath = format("%s%s.json", instructionsFileRemoteDirectory, instructionsFileName);

		ExtractorInstructionFilesDTO instructionsDto = new ExtractorInstructionFilesDTO();
		instructionsDto.setInstructionFileName(instructionsFileName);
		instructionsDto.setGobiiExtractorInstructions(instructions);

		extractorInstruction(instructionsDto);

		shell.execute(
				format("mv /data/gobii_bundle/crops/%s/extractor/instructions/%s.json " +
								"/data/gobii_bundle/crops/%s/extractor/inprogress/%s.json",
						crop, instructionsFileName,
						crop, instructionsFileName));

		shell.execute(format("(cd /data/gobii_bundle/core && java -Dorg.gobii.environment.name=BERT -jar Extractor.jar %s)", instructionsFileRemotePath));

		return instructionsDto;
	}

	@Verify("extract")
	public void verifyExtract(ExtractorInstructionFilesDTO instructionFilesDTO, ContactDTO pi, ProjectDTO project, ExperimentDTO experiment,
							  PlatformDTO platform, VendorProtocolDTO vendorProtocol, OrganizationDTO organization, ProtocolDTO protocol,
							  AnalysisDTO analysis, DataSetDTO dataset, String instructionFilePath, List<String> filesToCompare)
			throws Exception {

		String outputFolder = String.format("/data/gobii_bundle/crops/%s/extractor/output/%s/%s/%s/%s/",
				instructionFilesDTO.getGobiiExtractorInstructions().get(0).getGobiiCropType(),
				pi.getEmail().split("@")[0],
				instructionFilesDTO.getGobiiExtractorInstructions().get(0).getDataSetExtracts().get(0).getGobiiFileType().toString().toLowerCase(),
				instructionFilesDTO.getGobiiExtractorInstructions().get(0).getDataSetExtracts().get(0).getGobiiExtractFilterType().toString().toLowerCase(),
				instructionFilesDTO.getInstructionFileName()
		);

		for (String filePath : filesToCompare) {
			String fileType = new File(filePath).getName();
			String content = slurp(filePath);
			if ("summary.file".equals(fileType)) {
				content = generateSummaryFile(instructionFilesDTO, pi, dataset, platform, project);
			} else if ("project.file".equals(fileType)) {
				content = mungProjectFile(content, pi, project, experiment, platform, vendorProtocol, organization, protocol, analysis, dataset);
			} else if ("Dataset.hmp.txt".equals(fileType)) {
				content = mungeHapmapFile(dataset, content);
			}
			ExtractVerificationUtil.verifyFile(fileType, content, shell.slurp(outputFolder + new File(filePath).getName()));
		}
	}

	private String generateSummaryFile(ExtractorInstructionFilesDTO instructionFilesDTO, ContactDTO pi, DataSetDTO dataset, PlatformDTO platform, ProjectDTO project) {
		return format("%s\t%s\n%s\t%s\n%s\t%s\n%s\t%s\n%s\t%s\n%s\t%s\n%s\t%s\n",
			"Job ID", instructionFilesDTO.getInstructionFileName(),
			"Submit as", pi.getEmail(),
			"Format", StringUtils.capitalize(instructionFilesDTO.getGobiiExtractorInstructions().get(0).getDataSetExtracts().get(0).getGobiiFileType().toString().toLowerCase()),
			"Data Set", dataset.getDatasetName(),
			"Platform", platform.getPlatformName(),
			"Principal Investigator", pi.getUserName(),
			"Project", project.getProjectName());
	}

	private String mungeHapmapFile(DataSetDTO dataset, String content) throws Exception {
		StringBuilder sb = new StringBuilder();
		for (String line : content.split("\n")) {
			if (line.contains("dnasample_uuid")) {
				int leftPad = 0;
				String[] cells = line.split("\t");
				for (int i = 1 ; "".equals(cells[i]) ; i++, leftPad++);

				ResultSet rs = database.query(format("select dnasample_uuid, dnasample_name from getSampleQCMetadataByDataset(%s)", dataset.getId()));
				List<String> dnaSample = new LinkedList<>();
				while (rs.next()) {
					dnaSample.add(rs.getString("dnasample_uuid"));
				}
				String uuidLine = "#\t" + Stream.generate(() -> "").limit(leftPad).collect(Collectors.joining("\t")) + "\tdnasample_uuid\t"
						+ dnaSample.stream().collect(Collectors.joining("\t"));
				sb.append(uuidLine);
				sb.append("\n");
			} else {
				sb.append(line);
				sb.append("\n");
			}
		}
		return sb.toString();
	}

	private String mungProjectFile(String content, ContactDTO pi, ProjectDTO project, ExperimentDTO experiment,
								   PlatformDTO platform, VendorProtocolDTO vendorProtocol, OrganizationDTO organization,
								   ProtocolDTO protocol, AnalysisDTO analysis, DataSetDTO dataset) {
		Matrix<String> projectFile = ComponentsUtil.tsvToMatrix(content);

		Map<String, String> mungValues = new HashMap<>();
		mungValues.put("project_pi_contact", str(pi.getFirstName()) + " " + str(pi.getLastName()));
		mungValues.put("project_name", str(project.getProjectName()));
		mungValues.put("experiment_name", str(experiment.getExperimentName()));
		mungValues.put("platform_name", str(platform.getPlatformName()));
		mungValues.put("vendor_protocol_name", str(vendorProtocol.getName()));
		mungValues.put("vendor_name", str(organization.getName()));
		mungValues.put("protocol_name", str(protocol.getName()));
		mungValues.put("analysis_name", str(analysis.getAnalysisName()));
		mungValues.put("dataset_name", str(dataset.getDatasetName()));
		mungValues.put("dataset_type", "null".equals(dataset.getDatatypeName()) ? "" : str(dataset.getDatatypeName()));

		for (int i = 0 ; i < projectFile.width() ; i++) {
			if (mungValues.containsKey(projectFile.get(0, i))) {
				projectFile.set(1,i, mungValues.get(projectFile.get(0, i)));
			}
		}

		return ComponentsUtil.matrixToTsv(projectFile);
	}

	@Clean("extract")
	public void cleanExtract(ExtractorInstructionFilesDTO instructions, ContactDTO contact) throws Exception {
		database.sqlExecute(format("DELETE FROM job WHERE submitted_by = %s AND type_id IN (SELECT cv_id FROM cv WHERE term = 'extract')",
				contact.getId()));
	}

	private static Map<String, Function<GobiiLoaderProcedure, Integer>> idFetchers = new HashMap<>();

	static {
		idFetchers.put("platform_id", p -> p.getMetadata().getPlatform().getId());
		idFetchers.put("project_id", p -> p.getMetadata().getProject().getId());
		idFetchers.put("dataset_id", p -> p.getMetadata().getDataset().getId());
		idFetchers.put("map_id", p -> p.getMetadata().getMapset().getId());
		idFetchers.put("experiment_id", p -> p.getMetadata().getExperiment().getId());
	}

	private void fillInProcedurePrototype(GobiiLoaderProcedure procedure, ContactDTO contact, ProjectDTO project,
										  ExperimentDTO experiment, PlatformDTO platform, DataSetDTO dataset,
										  MapsetDTO mapset, String crop) {
		GobiiLoaderMetadata metadata = 		procedure.getMetadata();


		metadata
				.setContactId(contact.getId());
		metadata
				.setContactEmail(contact.getEmail());
		metadata
				.setGobiiJobStatus(JobProgressStatusType.CV_PROGRESSSTATUS_NOSTATUS);
		metadata
				.setProject(new PropNameId(project.getId(), project.getProjectName()));
		metadata
				.setExperiment(new PropNameId(experiment.getId(), experiment.getExperimentName()));
		metadata
				.setPlatform(new PropNameId(platform.getId(), platform.getPlatformName()));
		metadata
				.setDataset(new PropNameId(dataset.getDataSetId(), dataset.getDatasetName()));
		metadata
				.setGobiiCropType(this.crop);

		String randomString = randomString();

		procedure.getMetadata().getGobiiFile().setSource(
				format("/data/gobii_bundle/crops/%s/files/%s/", crop, randomString));

		procedure.getMetadata().getGobiiFile().setDestination(
				format("/data/gobii_bundle/crops/%s/loader/digest/%s", crop, randomString));

		if (mapset != null) {
			procedure.getMetadata().setMapset(new PropNameId(mapset.getId(), mapset.getName()));
		}

		for (GobiiLoaderInstruction instruction : procedure.getInstructions()) {
			for (GobiiFileColumn fc : instruction.getGobiiFileColumns()) {
				if (GobiiColumnType.CONSTANT.equals(fc.getGobiiColumnType())) {
					if (idFetchers.containsKey(fc.getName())) {
						fc.setConstantValue(idFetchers.get(fc.getName()).apply(procedure).toString());
					} else {
//						throw new RuntimeException(String.format("ID Fetcher for %s not found", fc.getName()));
					}

				}
			}
		}
	}

	@Action("digesterInstructionFilePath")
	public String digesterInstructionFilePath(String name, String status) {
		return format("/data/gobii_bundle/crops/%s/loader/%s/%s.json", crop, status, name);
	}

	@Action("extractorInstructionFilePath")
	public String extractorInstructionFilePath(String name, String status) {
		return format("/data/gobii_bundle/crops/%s/extractor/%s/%s.json", crop, status, name);
	}

	@Action("extractResultFiles")
	public List<String> extractResultFiles(ExtractorInstructionFilesDTO extract) {
		List<String> files = new LinkedList<>();

		for (GobiiExtractorInstruction instruction : extract.getGobiiExtractorInstructions()) {
			for (GobiiDataSetExtract dataSetExtract : instruction.getDataSetExtracts()) {
				files.add(dataSetExtract.getExtractDestinationDirectory());
			}
		}

		return files;
	}

	@Action("collectArtifacts")
	public void collectArtifacts(String folder, List<String> paths) throws Exception {

		for (String path : paths) {
			String dst = format("%s/%s", folder, path);
			String subFolder = dst.substring(0, dst.lastIndexOf("/"));
			shell.execute(format("mkdir -p %s", subFolder));
			shell.execute(format("cp -r %s %s", path, dst));
		}
	}

	@Action("zipRemote")
	public void zipRemote(String zipPath, List<String> files) throws Exception {
		String command = format("zip -r %s ") + String.join(" ", files);
		shell.execute(command);
	}

	@Action("scpFrom")
	public void scpFrom(String remotePath, String localPath) throws Exception {
		shell.scpRemote(remotePath, localPath);
	}

}
