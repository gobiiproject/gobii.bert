package org.gobii.bert;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static org.gobii.bert.Util.slurp;
import static org.junit.Assert.assertEquals;

public class ExtractVerificationUtil {


	private static Set<String> SAMPLE_IGNORE_COLUMNS = new HashSet<>();
	static {
		SAMPLE_IGNORE_COLUMNS.addAll(Arrays.asList("germplasm_id", "germplasm_seed_source_id", "gdm_germplasm_id", "dnasample_id", "dnarun_id"));
	}

	private static Set<String> MARKER_IGNORE_COLUMNS = new HashSet<>();
	static {
		MARKER_IGNORE_COLUMNS.addAll(Arrays.asList("variant_id", "marker_gene_id", "marker_id", "marker_clone_id"));
	}

	public static void verifySample(String knownGood, String result) {

		assertEquals("Sample comparison failed",
				project(parseTable(knownGood), SAMPLE_IGNORE_COLUMNS),
				project(parseTable(result), SAMPLE_IGNORE_COLUMNS));

	}

	public static void verifyMarker(String knownGood, String result) {
		assertEquals("Marker comparison failed",
				project(parseTable(knownGood), MARKER_IGNORE_COLUMNS),
				project(parseTable(result), MARKER_IGNORE_COLUMNS));
	}

	public static void verifyFile(String fileType, String knownGood, String resultContent) throws Exception {

		switch(fileType) {
			case "sample.file":
				verifySample(knownGood, resultContent);
				break;
			case "marker.file":
				verifyMarker(knownGood, resultContent);
				break;
			default:
				assertEquals(String.format("Comparision for %s failed", fileType),
						knownGood, resultContent);
		}
	}

	public static List<HashMap<String, String>> parseTable(String table) {
		String[] lines = table.split("\n");
		String[] columnNames = lines[0].split("\t");

		List<HashMap<String, String>> parsed = new ArrayList<>(lines.length - 1);

		for (int i = 1 ; i < lines.length ; i++) {
			HashMap<String, String> entry = new HashMap<>();
			String[] cells = lines[i].split("\t");
			for (int j = 0 ; j < columnNames.length ; j++) {
				entry.put(columnNames[j], cells[j]);
			}
		}

		return parsed;
	}

	public static List<HashMap<String, String>> project(List<HashMap<String, String>> table, Collection<String> columns) {
		table.forEach(r -> columns.forEach(c -> r.remove(c)));
		return table;
	}
}
