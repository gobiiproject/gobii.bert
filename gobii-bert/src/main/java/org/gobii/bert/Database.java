package org.gobii.bert;

import com.google.common.base.CaseFormat;
import ernie.core.Logger;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.gobiiproject.gobiimodel.dto.base.DTOBase;
import org.gobiiproject.gobiimodel.dto.annotations.GobiiEntityColumn;
import org.gobiiproject.gobiimodel.dto.noaudit.CvGroupDTO;
import org.gobiiproject.gobiimodel.dto.noaudit.DataSetDTO;

public class Database {

	private static Connection connection;

	public Connection getConnection() {
		return connection;
	}

	public void connect(String host, Integer port, String database, String user, String password) throws SQLException {
		connection = DriverManager.getConnection(
				String.format("jdbc:postgresql://%s:%s/%s", host, port, database),
				user, password);
	}

	public Set<String> getTableColumnNames(String table) throws SQLException {

		ResultSet metadata = connection.getMetaData()
				.getColumns(null,null, table, null);

		Set<String> columns = new HashSet<>();
		while (metadata.next()) {
			columns.add(metadata.getString("COLUMN_NAME"));
		}

		return columns;
	}

	public boolean tableExists(String table) throws SQLException {
		ResultSet tables = this.getConnection().getMetaData()
				.getTables(null, null, table, new String[] {"TABLE"});

		while(tables.next()) {
			if (tables.getString("TABLE_NAME").equals(table)) {
				return true;
			}
		}

		return false;
	}

	public PreparedStatement preparedStatement(String sql, Object ... args) throws SQLException {

		PreparedStatement statement = connection.prepareStatement(sql);

		for (int i = 0 ; i < args.length ; i++) {
			statement.setObject(i, args[i]);
		}

		return statement;
	}

	public void sqlExecute(String sql, Object ... args) throws SQLException {

		Logger.info(sql);

		try {
			preparedStatement(sql, args).execute();
		} catch (SQLException e) {
			throw new SQLException(String.format("%s : %s", sql, args), e);
		}
	}

	public ResultSet query(String sql, Object ... args) throws SQLException {
		Logger.info("QUERY: " + sql);
		try {
			return preparedStatement(sql, args).executeQuery();
		} catch (SQLException e) {
			throw new SQLException(String.format("%s : %s", sql, args), e);
		}
	}

	public <T> List<T> parseResultSet(Class<T> c, ResultSet rs) throws IllegalAccessException, InstantiationException, SQLException, InvocationTargetException {

		List<Method> entities = Arrays.stream(c.getDeclaredMethods())
				.filter(m -> m.isAnnotationPresent(GobiiEntityColumn.class))
				.collect(Collectors.toList());

		List<T> results = new LinkedList<>();

		while (rs.next()) {
			T t = c.newInstance();
			for (Method setter : entities) {
				setter.invoke(t, rs.getObject(setter.getAnnotation(GobiiEntityColumn.class).columnName()));
			}
			results.add(t);
		}

		return results;
	}

	public void delete(DTOBase dto) throws SQLException {

		String tableName = tableName(dto);

		sqlExecute(String.format("DELETE FROM %s WHERE %s_id = %s", tableName, tableName, dto.getId()));
	}

	private static Set<Class<?>> exceptions = new HashSet<>();
	static {
		exceptions.add(DataSetDTO.class);
		exceptions.add(CvGroupDTO.class);
	}

	private String tableName(DTOBase dto) {
		String className = dto.getClass().getSimpleName();
		className = className.substring(0, className.length() - 3); // Remove DTO

		if (exceptions.contains(dto.getClass())) {
			return className.toLowerCase();
		} else {
			return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, className);
		}
	}

}
