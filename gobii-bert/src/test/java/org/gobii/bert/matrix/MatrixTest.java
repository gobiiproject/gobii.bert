package org.gobii.bert.matrix;


import org.junit.Test;

import static org.junit.Assert.*;

public class MatrixTest {


	@Test
	public void testMatrixGetColumn() {
		Matrix<Integer> m = new Matrix<>(10,10);
		for (int i = 0 ; i < 10 ; i++) {
			m.set(i, 0, i);
		}

		Column<Integer> column = m.getColumn(0);
		for (int i = 0 ; i < 10 ; i++) {
			assertEquals((Object) i, column.get(i));
		}
	}

	@Test
	public void testColumnSlice() {
		Column<Integer> column = new Column<>(10);
		for (int i = 0 ; i < 10 ; i++) {
			column.set(i, i);
		}

		Column<Integer> slice = column.slice(5);
		assertEquals(5, slice.height());
		for (int i = 0 ; i < 5 ; i++) {
			assertEquals(column.get(i + 5), slice.get(i));
		}
	}
}
